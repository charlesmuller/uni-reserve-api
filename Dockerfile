FROM php:8.2-fpm

COPY nginx/snippets/fastcgi-php.conf /etc/nginx/snippets/
COPY . /app
WORKDIR /app

RUN if [ ! -e .env ]; then cp .env.example .env; fi

RUN apt-get update && apt-get install -y \
    libonig-dev \
    libzip-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    unzip \
    git \
    curl

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN composer install --no-interaction --no-progress
RUN composer update --no-interaction --no-progress
RUN apt-get install -y npm
RUN npm install
RUN npm run build

RUN php artisan key:generate --show > .env

RUN chown root:www-data storage -R && \
    chmod 775 storage -R
